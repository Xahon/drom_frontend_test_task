import React from "react";
import expect from "expect";
import { mount } from "enzyme";
import SelectList from "./SelectList";


describe("<SelectList />", () => {
    const w = mount(<SelectList name="city" values={["opt1"]} />);

    it("renders", () => {
        expect(w.find(SelectList).exists()).toBe(true);
    });

    it("should show placeholder", () => {
        expect(w.find("[jest-dom='placeholder']").text()).toBe("opt1");
    });
});