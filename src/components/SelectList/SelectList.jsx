import React, { useState, useEffect, useRef, useCallback } from "react";
import PropTypes from "prop-types";
import c from "classnames";
import * as css from "../../styles/Defaults.module.scss";
import * as styles from "./SelectList.module.scss";

const SelectList = (props) => {
    const {
        className,
        name,
        values,
        onChange,
        onBlur,
        disabled,
    } = props;
    if (values.length <= 0) {
        throw new Error("Must be given at least one option");
    }
    const rootRef = useRef();
    const [selectedOpt, setSelectedOpt] = useState(values[0]);
    const [expanded, setExpandedInternal] = useState(false);
    const [justMounted, setJustMounted] = useState(true);

    const setExpanded = (value) => {
        setJustMounted(false);
        setExpandedInternal(value);
    };

    const toggleExpandState = useCallback(() => {
        if (disabled)
            return;
        console.log("SelectList.jsx: toggleExpandState");
        setExpanded(!expanded);
    }, [disabled, expanded]);

    const handleMousedown = useCallback((e) => {
        console.log("SelectList.jsx: handleMousedown");
        if (expanded && !rootRef.current.contains(e.target)) {
            console.log("SelectList.jsx: handleMousedown = true");
            setExpanded(false);
        }
    }, [expanded]);

    useEffect(() => {
        console.log(`SelectList.jsx: OnChange '${selectedOpt}'`);
        onChange(selectedOpt);
    }, [onChange, selectedOpt]);

    useEffect(() => {
        console.log("SelectList.jsx: Add mousedown event listener");
        document.addEventListener("mousedown", handleMousedown);
        return () => {
            console.log("SelectList.jsx: Remove mousedown event listener");
            document.removeEventListener("mousedown", handleMousedown);
        };
    }, [handleMousedown]);

    useEffect(() => {
        if (!expanded && !justMounted)
            onBlur();
    }, [expanded, justMounted, onBlur]);

    return (
        <div
            ref={rootRef}
            className={c(
                styles.select,
                {
                    [styles.expanded]: expanded,
                    [styles.disabled]: disabled,
                },
                className,
            )}
            onClick={toggleExpandState}
            onKeyDown={toggleExpandState}
            role="button"
            tabIndex={0}
        >
            <div
                className={c(
                    styles.optionBase,
                    styles.placeholderOption,
                    styles.optionWithArrow)}
                jest-dom="placeholder"
            >
                {selectedOpt}
            </div>
            <ul className={c(styles.optionsList)}>
                {values.map((value) => {
                    return (
                        <li key={value}>
                            <button
                                type="button"
                                value={value}
                                onClick={() => setSelectedOpt(value)}
                                className={c(
                                    css.input,
                                    styles.optionBase,
                                    styles.option,
                                    { [styles.optionSelected]: value === selectedOpt })}
                                disabled={disabled}
                            >
                                {value}
                            </button>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};

SelectList.propTypes = {
    name: PropTypes.string.isRequired,
    values: PropTypes.arrayOf(PropTypes.string).isRequired,
    className: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    disabled: PropTypes.bool,
};

SelectList.defaultProps = {
    className: "",
    onChange: () => { },
    onBlur: () => { },
    disabled: false,
};

export default SelectList;