import React, { useEffect, useState, useCallback } from "react";
import c from "classnames";
import logo from "../../assets/images/logo.svg";
import * as css from "../../styles/Defaults.module.scss";
import * as styles from "./OnlineRecordingForm.module.scss";

import Select from "../SelectList/SelectList";
import LoadingSpinner from "../LoadingSpinner/LoadingSpinner";

const OnlineRecordingForm = () => {
  const [submitting, setSubmitting] = useState(false);

  useEffect(() => {
    if (submitting) { }
  }, [submitting]);

  const onFormSubmit = useCallback((e) => {
    e.preventDefault();
    setSubmitting(true);
  }, [setSubmitting]);

  return (
    <div className={c(styles.main)}>
      <div className={c(styles.logoWrapper)}>
        <img className={c(styles.logo)} src={logo} alt="ffoo" />
        {
          submitting &&
          <LoadingSpinner className={c(styles.logoLoader)} />
        }
      </div>
      <p className={c(css.title)}>Онлайн запись</p>
      <form className={c(styles.form)} onSubmit={onFormSubmit}>
        <Select
          name="city"
          values={[
            "Владивосток",
            "test1",
            "test2",
          ]}
          disabled={submitting}
        />
        <div className={c(styles.infoBlock)}>
          <p className={c(css.text, css.textSmall)}>ул. Первая 1, ст. 1</p>
          <a className={c(css.text, css.textSmall, css.anchor)} href="tel:+79999999999">+7 (999) 999-99-99</a>
          <p className={c(css.text, css.textSmall)}>Стоимость услуги 12000₽</p>
        </div>
        <div className={c(styles.formTwoPanes)}>
          <Select
            className={c(css.input, styles.formTwoPanesChild)}
            name="date"
            values={[
              "Дата",
            ]}
            disabled={submitting}
          />
          <Select
            className={c(css.input, styles.formTwoPanesChild)}
            name="time"
            values={[
              "Время",
            ]}
            disabled={submitting}
          />
        </div>

        <div className={c(styles.rowPadding)}>
          <input
            className={c(css.input)}
            type="text"
            name="number"
            placeholder="+7 (___) ___-__-__"
            disabled={submitting}
          />
        </div>
        <div className={c(styles.rowPadding)}>
          <input
            className={c(css.input)}
            type="text"
            name="name"
            placeholder="Ваше имя"
            disabled={submitting}
          />
        </div>
        <div className={c(styles.rowPaddingButton, styles.rowCenter)}>
          <input
            className={c(css.input,
              css.button)}
            type="submit"
            name="Записаться"
            value="Записаться"
            disabled={submitting}
          />
        </div>
        <div className={c(styles.rowPaddingPrivacy)}>
          <p className={c(styles.textPrivacy, css.textPale, css.textSmaller)}>
            Нажимая &quot;Записаться&quot;, я выражаю свое согласие с обработкой моих персональных данных
          в соответствии с принятой <a className={c(css.text, css.textSmaller, css.anchor)} href="#">политикой конфиденциальности</a> и
          принимаю <a className={c(css.text, css.textSmaller, css.anchor)} href="#">пользовательское соглашение</a>
          </p>
        </div>
      </form>
    </div>
  );
};

export default OnlineRecordingForm;
