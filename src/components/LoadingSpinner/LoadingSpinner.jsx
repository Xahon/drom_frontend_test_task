import React from "react";
import PropTypes from "prop-types";
import c from "classnames";
import * as styles from "./LoadingSpinner.scss";

const LoadingSpinner = (props) => {
    const {
        className,
    } = props;

    return (
        <div className={c(styles.ldsRing, className)}><div /><div /><div /><div /></div>
    );
};

LoadingSpinner.propTypes = {
    className: PropTypes.string,
};

LoadingSpinner.defaultProps = {
    className: "",
};

export default LoadingSpinner;
