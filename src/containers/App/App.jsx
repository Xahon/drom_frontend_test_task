import React from "react";
import c from "classnames";
import OnlineRecordingForm from "../../components/OnlineRecordingForm/OnlineRecordingForm";
import styles from "./App.module.scss";

const App = () => {
  return (
    <div className={c(
      styles.center,
      styles.fullscreen)}
    >
      <OnlineRecordingForm />
    </div>
  );
};

export default App;
