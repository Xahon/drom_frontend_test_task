import React from "react";
import OnlineRecordingForm from "../components/OnlineRecordingForm/OnlineRecordingForm";
import styles from "./App.scss";

const App = (props) => {
  return (
    <>
      <div className={c(
        styles.center,
        styles.fullscreen)}
      >
        <OnlineRecordingForm />
      </div>
    </>
  );
};

export default App;
